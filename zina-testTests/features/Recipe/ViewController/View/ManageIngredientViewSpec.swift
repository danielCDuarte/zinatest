//
//  ManageIngredientViewSpec.swift
//  zina-testTests
//
//  Created by daniel.crespo on 1/24/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import zina_test

class ManageIngredientViewSpec: QuickSpec {
    override func spec() {
        describe("ManageIngredientViewSpec") {
            
            var sut: ManageIngredientView!
            var mainVC: UIViewController!
            beforeEach {
                var delegate: ManageIngredientViewDelegate?
                
                sut = ManageIngredientView(delegate: delegate)
                                
                mainVC = UIViewController()
                mainVC.view = sut
                mainVC.view.backgroundColor = .white
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View controller is instanciated", {
                it("Should be a valid snapshot add", closure: {
                    
                    sut.setupView(ingredient: nil, typeManage: .add)
                    expect(UIWindow.testWindow) == recordSnapshot("ManageIngredientViewSpecAdd")
                })
                
                it("Should be a valid snapshot edit", closure: {
                    
                    sut.setupView(ingredient: Ingredient(id: "1", type: "apple"), typeManage: .edit)
                    expect(UIWindow.testWindow) == recordSnapshot("ManageIngredientViewSpecEdit")
                })
            })
        }
    }
}
