//
//  RecipeListProtocols.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit

protocol RecipeListViewProtocol: class {
    // PRESENTER -> VIEW
    func showIngredients(with ingredients: [Ingredient])
    func showMessage(title:String,message:String)
}

protocol RecipeListPresenterProtocol: class {
    //View -> Presenter
    var interactor: RecipeListInputInteractorProtocol? {get set}
    var view: RecipeListViewProtocol? {get set}
    var wireframe: RecipeListWireFrameProtocol? {get set}
    
    func viewDidLoad()
    func deleteIngredient(ingredient:Ingredient)
    func manageIngredient(with ingredient: Ingredient?,typeManage: TypeManage, from view: UIViewController)
}

protocol RecipeListInputInteractorProtocol: class {
    var presenter: RecipeListOutputInteractorProtocol? {get set}
    //Presenter -> Interactor
    func getIngredients()
    func deleteIngredient(ingredient:Ingredient)
}

protocol RecipeListOutputInteractorProtocol: class {
    //Interactor -> Presenter
    func ingredientsDidFetch(ingredientsList: [Ingredient])
    func showMessage(title:String,message:String)
}

protocol RecipeListWireFrameProtocol: class {
    //Presenter -> Wireframe
    func pushToManageIngredient(with ingredient: Ingredient?, typeManage: TypeManage , from view: UIViewController)
    static func createRecipeModule(recipeRef: RecipeListViewController)
}
