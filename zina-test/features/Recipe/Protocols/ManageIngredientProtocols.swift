//
//  ManageIngredientProtocols.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit

protocol ManageIngredientViewProtocol: class {
    //Presenter -> View
    func showManageIngredient(with detail: Ingredient?)
    func showMessage(title:String,message:String)
    
}

protocol ManageIngredientPresenterProtocol: class {
    //View -> Presenter
    
    var interactor: ManageIngredientInputInteractorProtocol? {get set}
    var wireframe: ManageIngredientWireFrameProtocol? {get set}
    var view: ManageIngredientViewProtocol? {get set}
    
    func viewDidLoad(ingredient:Ingredient?)
    func manageIngredient(ingredient:Ingredient,typeManage:TypeManage, id:String)
    
}

protocol ManageIngredientInputInteractorProtocol: class {
    var presenter: ManageIngredientOutputInteractorProtocol? {get set}
    //Presenter -> Interactor
    func manageIngredient(ingredient:Ingredient ,typeManage:TypeManage, id: String)
}

protocol ManageIngredientOutputInteractorProtocol: class {
    //Interactor -> Presenter
    func showMessage(title:String,message:String)
}

protocol ManageIngredientWireFrameProtocol: class {
    func goBackToPayView(from view: UIViewController)
    static func createManageIngredientModule(manageIngredientRef: ManageIngredientViewController,ingredient: Ingredient?)
}
