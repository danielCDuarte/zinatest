//
//  ManageIngredientInteractor.swift
//  zina-test
//
//  Created by daniel.crespo on 1/24/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import Foundation

class ManageIngredientInteractor: ManageIngredientInputInteractorProtocol {
    
    
    
    var database = dbApp(database: "zina", ext: "db")
    
    weak var presenter: ManageIngredientOutputInteractorProtocol?
    
    func manageIngredient(ingredient: Ingredient, typeManage: TypeManage, id: String) {
        switch typeManage {
        case .add:
            database.insert(table: "ingredient", params: [], values: [ingredient.id,ingredient.type], sucess: { (result) in
                self.presenter?.showMessage(title: "", message: NSLocalizedString("addSuccess", comment: ""))
            }) { (error) in
                self.presenter?.showMessage(title: "", message: NSLocalizedString("addFailure", comment: ""))
            }
        case .edit:
            database.updateById(table: "ingredient", values: [ingredient.id,ingredient.type], id: id, sucess: { (successResult) in
                self.presenter?.showMessage(title: "", message: NSLocalizedString("editSuccess", comment: ""))
            }) { (Error) in
                self.presenter?.showMessage(title: "", message: NSLocalizedString("editFailure", comment: ""))
            }
        case .delete:
            return
        default:
            return
        }
    }
    
}

