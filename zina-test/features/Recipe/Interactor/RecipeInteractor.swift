//
//  RecipeInteractor.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import Foundation
import Alamofire
import Network

class RecipeInteractor: RecipeListInputInteractorProtocol {
    
    var database = dbApp(database: "zina", ext: "db")
    weak var presenter: RecipeListOutputInteractorProtocol?
    
    func getIngredients() {
        
        self.persistenceIngredients { (listIngredients) in
            self.presenter?.ingredientsDidFetch(ingredientsList: listIngredients)
        }
    }
    
    func deleteIngredient(ingredient: Ingredient) {
        database.deleteById(table: "ingredient", id: ingredient.id, sucess: { (successResult) in
            self.presenter?.showMessage(title: "", message: NSLocalizedString("deleteSuccess", comment: ""))
        }) { (Error) in
            self.presenter?.showMessage(title: "", message: NSLocalizedString("deleteFailure", comment: ""))
        }
    }
    func getJSON(url:String) -> NSData {
        return NSData(contentsOf: NSURL(string: url)! as URL)!
    }
    
    func persistenceIngredients(completion: @escaping ([Ingredient]) -> Void){
        
        let defaults = UserDefaults.standard
        
        if defaults.string(forKey: "isPersistence") != nil{
            //traer datos de sqlite
            var Ingredients = [Ingredient]()
            database.selectAll(table: "ingredient", sucess: { (queryResult) in
                print("queryResult",queryResult)
                let itemsCount = queryResult.results?.count ?? 0
                for n in 0..<itemsCount{
                    let type:String = (queryResult.results?[n]["type"] as? String)!
                    let id:String = (queryResult.results?[n]["id"] as? String)!
                    Ingredients.append(Ingredient(id: id, type: type))
                }
            }) { (error) in
                print(error.localizedDescription)
            }
            completion(Ingredients)
            return
        }
        
        //Almacenar datos a sqlite
        self.getAllIngredients { (response) in
            defaults.set(response?.name, forKey: "titleRecipe")
            if let ingredients = response?.ingredients{
                for ingredient in ingredients{
                    self.database.insert(table: "ingredient", params: ["id","type"], values: [ingredient.id,ingredient.type], sucess: { (SQLiteQueryResult) in
                        print("Add ingredient")
                    }) { (Error) in
                        print("error",Error)
                    }
                }
                defaults.set(true, forKey: "isPersistence")
                completion(ingredients)
            }
        }
        
    }
    
    func getAllIngredients(completion: @escaping (ResponseRecipe?) -> Void) {
        
        Alamofire.request("http://www.mocky.io/v2/5ddbebf23400008e4feadfcc", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result{
            case .success(let value):
                let dataConvert = value as! NSArray
                if let dataIndex = dataConvert[0] as? NSDictionary{
                    let identifier = dataIndex.object(forKey: "id") as! String
                    let type = dataIndex.object(forKey: "type") as! String
                    let name = dataIndex.object(forKey: "name") as! String
                    let ppu = dataIndex.object(forKey: "ppu") as! Double
                    
                    var Ingredients = [Ingredient]()
                    let toppings = dataIndex.object(forKey: "topping") as! NSArray
                    for i in 0..<toppings.count {
                        let indexToppings = toppings[i] as! NSDictionary
                        Ingredients.append(Ingredient(id: indexToppings.object(forKey: "id") as! String, type: indexToppings.object(forKey: "type") as! String))
                    }
                    
                    let batters = dataIndex.object(forKey: "batters") as! NSDictionary
                    let batterIndex = batters.object(forKey: "batter")as! NSArray
                    for e in 0..<batterIndex.count {
                        let indexbatter = batterIndex[e] as! NSDictionary
                        Ingredients.append(Ingredient(id: indexbatter.object(forKey: "id") as! String, type: indexbatter.object(forKey: "type") as! String))
                    }
                    
                    let responseRecipe:ResponseRecipe = ResponseRecipe(identifier: identifier, type: type, name: name, ppu: ppu, ingredients: Ingredients)
                    completion(responseRecipe)
                }else{
                    completion(nil)
                    return
                }
                
            case .failure(let error):
                print("error",error)
                completion(nil)
            }
        }
        
    }
    
}
