//
//  ManageIngredientViewController.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit

class ManageIngredientViewController: UIViewController, ManageIngredientViewProtocol{
    
    
    // MARK: - Properties
    
    private lazy var manageIngredientView: ManageIngredientView = {
        let view = ManageIngredientView(delegate: self)
        return view
    }()
    
    // MARK: - init
    var presenter: ManageIngredientPresenterProtocol?
    private var typeManage:TypeManage
    var ingredient: Ingredient?
    var identifier: String = ""
    init(typeManage:TypeManage) {
        self.typeManage = typeManage
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = typeManage == .add ? NSLocalizedString("add", comment: ""): NSLocalizedString("edit", comment: "")
        presenter?.viewDidLoad(ingredient: self.ingredient)
        self.view = manageIngredientView
        
        if let identifier = self.ingredient?.id{
            self.identifier = identifier
            self.manageIngredientView.idTextField.isEnabled = false
        }
    }
    
    func showManageIngredient(with detail: Ingredient?) {
        manageIngredientView.setupView(ingredient: detail, typeManage: self.typeManage)
    }
    
    func showMessage(title:String,message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("btnAccept", comment: ""), style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
}

extension ManageIngredientViewController: ManageIngredientViewDelegate{
    func showMessageValidate() {
        self.showMessage(title: "", message: NSLocalizedString("validateMessage", comment: ""))
    }
    
    func manageIngredient(type: TypeManage, ingredient: Ingredient) {
        self.presenter?.manageIngredient(ingredient: ingredient, typeManage: type, id: self.identifier)
    }
}
