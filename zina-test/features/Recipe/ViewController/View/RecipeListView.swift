//
//  RecipeListView.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit
import SnapKit
import Reusable

class RecipeListView: UIView {
    
    // MARK: - properties
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView()
        
        return tableView
    }()
    
    // MARK: - init
    private weak var delegate: RecipeListViewDelegate?
    
    init(delegate: RecipeListViewDelegate?) {
        self.delegate = delegate
        super.init(frame: .zero)
        setupViewConfiguration()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 50.0
        tableView.separatorStyle = .none
        
        tableView.register(cellType: IngredientTableViewCell.self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - setup
     var ingredientList = [Ingredient]()
    func setup(ingredientList:[Ingredient]){
        self.ingredientList = ingredientList
        self.tableView.reloadData()
    }
    
    func validateListCheck()-> Bool{
        var countValidate:Int = 0
        for ingredientItem in self.ingredientList {
            if ingredientItem.select == true {
                countValidate += 1
            }
        }
        if self.ingredientList.count == countValidate{
            return true
        }
        return false
    }
}

// MARK: - View Configuration
extension RecipeListView: ViewConfiguration {
    func configureViews() {
        self.backgroundColor = .white
        self.tableView.backgroundColor = .white
    }
    func buildViewHierarchy() {
        self.addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }
    }

}

extension RecipeListView: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.ingredientList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ingredient = self.ingredientList[indexPath.row]
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: IngredientTableViewCell.self)
        cell.configure(with: ingredient)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! IngredientTableViewCell
        cell.selectCell(state: !cell.select)
        self.ingredientList[indexPath.row].select = cell.select
        
        if self.validateListCheck(){
            self.delegate?.selectAllItems()
        }
        
    }
    
    // MARK: - Swipe Cell
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let ingredientSwipe:Ingredient = self.ingredientList[indexPath.row]
        let delete = UIContextualAction(style: .destructive, title: NSLocalizedString("delete", comment: "")) {  (contextualAction, view, boolValue) in
            self.delegate?.didDelete(ingredient: ingredientSwipe)
        }
        
        let edit = UIContextualAction(style: .normal, title: NSLocalizedString("edit", comment: "")) {  (contextualAction, view, boolValue) in
            self.delegate?.didEdit(ingredient: ingredientSwipe)
        }
        edit.backgroundColor = UIColor.green
        
        let swipeActions = UISwipeActionsConfiguration(actions: [delete, edit])

        return swipeActions
    }
    
}
