//
//  ManageIngredientView.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit
import SnapKit

enum TypeManage: String {
    case add = "Add"
    case edit = "Edit"
    case delete = "Delete"
}
class ManageIngredientView: UIView {
    // MARK: - properties
    
    private(set) lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.backgroundColor = .white
        return view
    }()
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    private(set) lazy var idTextField: UITextField = {
        let textfield = UITextField()
        textfield.borderStyle = .roundedRect
        textfield.textColor = Colors.blue
        textfield.keyboardType = .numberPad
        textfield.placeholder = NSLocalizedString("idTextPlaceholder", comment: "")
        return textfield
    }()
    
    private(set) lazy var nameTextField: UITextField = {
        let textfield = UITextField()
        textfield.borderStyle = .roundedRect
        textfield.textColor = Colors.blue
        textfield.keyboardType = .alphabet
        textfield.placeholder = NSLocalizedString("nameTextPlaceholder", comment: "")
        return textfield
    }()
    
    private(set) lazy var savebutton: UIButton = {
        let button = UIButton()
        button.cornerRadius = 4
        button.addTarget(self, action: #selector(actionManage(_:)), for: .touchUpInside)
        button.setTitleColor(Colors.white, for: .normal)
        return button
    }()
    
    // MARK: - init
    private weak var delegate: ManageIngredientViewDelegate?
    
    
    init(delegate: ManageIngredientViewDelegate?) {
        self.delegate = delegate
        
        super.init(frame: .zero)
        setupViewConfiguration()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - Setup View
    private var ingredient: Ingredient?
    private var typeManage:TypeManage?
    func setupView(ingredient:Ingredient?, typeManage:TypeManage){
        self.typeManage = typeManage
       
        if let ingredient = ingredient{
            self.ingredient = ingredient
            self.idTextField.text = ingredient.id
            self.nameTextField.text = ingredient.type
        }
        self.savebutton.setTitle(typeManage == .add ? NSLocalizedString("add", comment: ""): NSLocalizedString("edit", comment: ""), for: .normal)
        self.savebutton.backgroundColor = typeManage == .add ? UIColor.blue : UIColor.green
    }
    
    // MARK: - Actions
    @objc func actionManage(_ sender : UIButton){
        if self.idTextField.text != "", self.nameTextField.text != ""{
            if let identifier = self.idTextField.text ,let name = self.nameTextField.text, let typeManage = self.typeManage{
                let ingredientManage = Ingredient(id: identifier, type: name)
                self.delegate?.manageIngredient(type: typeManage, ingredient: ingredientManage)
            }else{
                self.delegate?.showMessageValidate()
            }
        }else{
            self.delegate?.showMessageValidate()
        }
        
    }
    
}

// MARK: - View Configuration
extension ManageIngredientView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(idTextField)
        containerView.addSubview(nameTextField)
        containerView.addSubview(savebutton)
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().priority(.low)
        }
        
        idTextField.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.height.equalTo(40)
        }
        
        nameTextField.snp.makeConstraints { (make) in
            make.top.equalTo(idTextField.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.height.equalTo(40)
        }
        
        savebutton.snp.makeConstraints { (make) in
            make.height.equalTo(48)
            make.width.equalTo(200)
            make.top.equalTo(nameTextField.snp.bottom).offset(16)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-16)
        }
    }

}
