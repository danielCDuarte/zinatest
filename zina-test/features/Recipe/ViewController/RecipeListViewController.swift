//
//  RecipeListViewController.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit

class RecipeListViewController: UIViewController,RecipeListViewProtocol{
    
    // MARK: - Properties
    
    var presenter:RecipeListPresenterProtocol?
    var ingredientList = [Ingredient]()
    private var ingredientManage :Ingredient?
    
    private lazy var recipeListView: RecipeListView = {
        let view = RecipeListView(delegate: self)
        return view
    }()
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: NSLocalizedString("add", comment: ""), style: .done, target: self, action: #selector(self.action(sender:)))
        RecipeListWireframe.createRecipeModule(recipeRef: self)
        self.view = recipeListView
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.showBlurLoader()
        presenter?.viewDidLoad()
    }
    
    func showIngredients(with ingredients: [Ingredient]) {
        ingredientList = ingredients
        if let title = UserDefaults.standard.string(forKey: "titleRecipe"){
            self.title = title
        }
        
        self.recipeListView.setup(ingredientList: ingredientList)
        self.view.removeBluerLoader()
    }
    //MARK: - Actions
    @objc func action(sender: UIBarButtonItem) {
        // Function body goes here
        self.didAdd()
    }
    
    func showMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("btnAccept", comment: ""), style: .default, handler: {(alert: UIAlertAction!) in
            self.view.showBlurLoader()
            self.presenter?.viewDidLoad()
        }))
        self.present(alert, animated: true)
    }
    
    
}

extension RecipeListViewController: RecipeListViewDelegate{
    func didAdd() {
        self.presenter?.manageIngredient(with: nil, typeManage: .add, from: self)
    }
    
    func didEdit(ingredient: Ingredient) {
        self.presenter?.manageIngredient(with: ingredient, typeManage: .edit, from: self)
    }
    
    func didDelete(ingredient: Ingredient) {
        self.presenter?.deleteIngredient(ingredient: ingredient)
    }
    
    func selectAllItems() {
        
        self.showMessage(title: "", message: NSLocalizedString("successRecipe", comment: ""))
    }
    
}
