//
//  ManageIngredientViewDelegate.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import Foundation

protocol ManageIngredientViewDelegate: class {
    func manageIngredient(type:TypeManage,ingredient:Ingredient)
    func showMessageValidate()
}
