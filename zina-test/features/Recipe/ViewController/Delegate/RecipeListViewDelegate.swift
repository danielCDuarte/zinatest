//
//  RecipeListViewDelegate.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import Foundation

protocol RecipeListViewDelegate: class {
    func selectAllItems()
    func didAdd()
    func didEdit(ingredient:Ingredient)
    func didDelete(ingredient:Ingredient)
}

