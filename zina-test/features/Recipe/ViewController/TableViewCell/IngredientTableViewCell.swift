//
//  IngredientTableViewCell.swift
//  zina-test
//
//  Created by daniel.crespo on 1/22/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit
import SnapKit
import Reusable

class IngredientTableViewCell: UITableViewCell, Reusable{
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 10
        return stackView
    }()
    
    private lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkText
        label.numberOfLines = 0
        return label
    }()
    
    var select:Bool = false
    // MARK: - init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        setupViewConfiguration()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - View Configuration

extension IngredientTableViewCell: ViewConfiguration {
    func configureViews() {
        backgroundColor = .white
    }
    
    func configure(with ingredient:Ingredient) {
        self.titleLabel.text = ingredient.type
        self.iconImageView.image = UIImage(named: self.select ? "check":"uncheck")
    }
    
    func selectCell(state:Bool){
        self.select = state
        self.iconImageView.image = UIImage(named: self.select ? "check":"uncheck")
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(stackView)
        contentView.addSubview(bottomLine)
        stackView.addArrangedSubview(iconImageView)
        stackView.addArrangedSubview(titleLabel)
        
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints { (make) in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalTo(bottomLine.snp.top)
        }
        
        bottomLine.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
        iconImageView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(10)
            make.height.equalTo(20)
            make.width.equalTo(20)
        }
    }

}
