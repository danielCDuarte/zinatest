//
//  ManageIngredientPresenter.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit

class ManageIngredientPresenter: ManageIngredientPresenterProtocol {
    
    
    
    var interactor: ManageIngredientInputInteractorProtocol?
    var view: ManageIngredientViewProtocol?
    var wireframe: ManageIngredientWireFrameProtocol?
    var presenter: ManageIngredientPresenterProtocol?
    
    
    func viewDidLoad(ingredient:Ingredient?) {
        view?.showManageIngredient(with: ingredient)
    }
    
    func manageIngredient(ingredient: Ingredient, typeManage: TypeManage, id: String) {
        interactor?.manageIngredient(ingredient: ingredient, typeManage: typeManage, id: id)
    }
    
}

extension ManageIngredientPresenter: ManageIngredientOutputInteractorProtocol{
    func showMessage(title: String, message: String) {
        view?.showMessage(title: title, message: message)
    }
}
