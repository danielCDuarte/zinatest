//
//  RecipeListPresenter.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit

class RecipeListPresenter: RecipeListPresenterProtocol {
    
    var interactor: RecipeListInputInteractorProtocol?
    var view: RecipeListViewProtocol?
    var wireframe: RecipeListWireFrameProtocol?
    var presenter: RecipeListPresenterProtocol?
    
    func viewDidLoad() {
        self.loadIngredientList()
    }
    
    func loadIngredientList() {
        interactor?.getIngredients()
    }
    
    func manageIngredient(with ingredient: Ingredient?,typeManage: TypeManage, from view: UIViewController) {
        wireframe?.pushToManageIngredient(with: ingredient, typeManage: typeManage, from: view)
    }
    
    func deleteIngredient(ingredient: Ingredient) {
        interactor?.deleteIngredient(ingredient: ingredient)
    }
    
}

extension RecipeListPresenter:RecipeListOutputInteractorProtocol {
    
    
    func ingredientsDidFetch(ingredientsList: [Ingredient]) {
        view?.showIngredients(with: ingredientsList)
    }
    
    func showMessage(title: String, message: String) {
        view?.showMessage(title: title, message: message)
    }
    
    
}
