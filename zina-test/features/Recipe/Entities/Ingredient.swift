//
//  Ingredient.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import Foundation

class Ingredient : Codable{
    var id: String
    var type: String
    var select:Bool?
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(String.self, forKey: .id)
        type = try values.decode(String.self, forKey: .type)
    }
    init(id: String, type: String, select:Bool = false) {
        self.type = type
        self.id = id
        self.select = select
    }
}
