//
//  ResponseRecipe.swift
//  zina-test
//
//  Created by daniel.crespo on 1/22/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//
import Foundation

class ResponseRecipe: Codable {
    let identifier: String
    let type: String
    let name: String
    let ppu: Double?
    let ingredients: [Ingredient]
    
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case type = "type"
        case name = "name"
        case ppu = "ppu"
        case ingredients = "batters"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        identifier = try values.decode(String.self, forKey: .identifier)
        type = try values.decode(String.self, forKey: .type)
        name = try values.decode(String.self, forKey: .name)
        ppu = try values.decode(Double.self, forKey: .ppu)
        ingredients = try values.decode([Ingredient].self, forKey: .ingredients)
    }
    
    init(identifier: String, type: String, name: String, ppu: Double, ingredients: [Ingredient]) {
        self.identifier = identifier
        self.type = type
        self.name = name
        self.ppu = ppu
        self.ingredients = ingredients
    }
    
}
