//
//  RecipeListWireframe.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit

class RecipeListWireframe: RecipeListWireFrameProtocol {
    
    static func createRecipeModule(recipeRef: RecipeListViewController) {
        let presenter: RecipeListPresenterProtocol & RecipeListOutputInteractorProtocol = RecipeListPresenter()
        recipeRef.presenter = presenter
        recipeRef.presenter?.wireframe = RecipeListWireframe()
        recipeRef.presenter?.view = recipeRef
        recipeRef.presenter?.interactor = RecipeInteractor()
        recipeRef.presenter?.interactor?.presenter = presenter
        
    }
    
    func pushToManageIngredient(with ingredient: Ingredient?, typeManage: TypeManage , from view: UIViewController) {
        let manageIngredientVC = ManageIngredientViewController(typeManage: typeManage)
        ManageIngredientWireframe.createManageIngredientModule(manageIngredientRef: manageIngredientVC, ingredient: ingredient)
        view.navigationController?.pushViewController(manageIngredientVC, animated: true)
    }
    
}
