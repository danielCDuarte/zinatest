//
//  ManageIngredientWireframe.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import UIKit

class ManageIngredientWireframe: ManageIngredientWireFrameProtocol {

    static func createManageIngredientModule(manageIngredientRef: ManageIngredientViewController,ingredient: Ingredient?) {
        let presenter: ManageIngredientPresenterProtocol & ManageIngredientOutputInteractorProtocol = ManageIngredientPresenter()
        manageIngredientRef.presenter = presenter
        manageIngredientRef.ingredient = ingredient
        manageIngredientRef.presenter?.wireframe = ManageIngredientWireframe()
        manageIngredientRef.presenter?.view = manageIngredientRef
        manageIngredientRef.presenter?.interactor = ManageIngredientInteractor()
        manageIngredientRef.presenter?.interactor?.presenter = presenter
    }
    
    func goBackToPayView(from view: UIViewController) {
        
    }
    
}
