//
//  Colors.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    static let blue = #colorLiteral(red: 0.0431372549, green: 0.368627451, blue: 0.5764705882, alpha: 1)
    static let red = #colorLiteral(red: 0.9803921569, green: 0, blue: 0.003921568627, alpha: 1)
    static let redHidden = #colorLiteral(red: 0.9803921569, green: 0.4812395873, blue: 0.5441295971, alpha: 1)
    static let DarkGray = #colorLiteral(red: 0.07450980392, green: 0.06274509804, blue: 0.05490196078, alpha: 1)
    static let Background = #colorLiteral(red: 0.937254902, green: 0.9137254902, blue: 0.8980392157, alpha: 1)
    static let white = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let black_opacity = UIColor.create(0, 0, 0, 0.3)
}
