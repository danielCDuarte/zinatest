//
//  DbApp.swift
//  zina-test
//
//  Created by daniel.crespo on 1/21/20.
//  Copyright © 2020 daniel.crespo. All rights reserved.
//

import Foundation
import SQLiteManager

class dbApp {
    var database:SQLite!
    
    init(database:String,ext:String) {
        self.database = try? SQLitePool.manager().initialize(database: database, withExtension: ext)
    }
    
    //MARK: Functions bd sqlite
    func selectAll(table:String,sucess: @escaping (_ result: SQLiteQueryResult)->(),failure: @escaping (_ error: Error)->()){
        do{
            let result = try database.query("select * from \(table);")
            if (result.results != nil) {
                if ((result.results?.count)! > 0) {
                    sucess(result)
                }else{
                    failure(NSError(domain:"error no data", code:1, userInfo:nil))
                }
            }else{
                failure(NSError(domain:"error no data", code:1, userInfo:nil))
            }
        } catch let error {
            failure(error)
        }
    }
    
    func selectCondition(table:String,condition:String,sucess: @escaping (_ result: SQLiteQueryResult)->(),failure: @escaping (_ error: Error)->()){
        do{
            let result = try database.query("select * from \(table) Where \(condition);")
            if (result.results != nil) {
                if ((result.results?.count)! > 0) {
                    sucess(result)
                }else{
                    failure(NSError(domain:"error no data", code:1, userInfo:nil))
                }
            }else{
                failure(NSError(domain:"error no data", code:1, userInfo:nil))
            }
        } catch let error {
            failure(error)
        }
        
    }
    
    func execQuery(query:String,sucess: @escaping (_ result: SQLiteQueryResult)->(),failure: @escaping (_ error: Error)->()){
        do{
            let result = try database.query("\(query);")
            if (result.results != nil) {
                if ((result.results?.count)! > 0) {
                    sucess(result)
                }else{
                    failure(NSError(domain:"error no data", code:1, userInfo:nil))
                }
            }else{
                failure(NSError(domain:"error no data", code:1, userInfo:nil))
            }
        } catch let error {
            failure(error)
        }
    }
    
    func deleteAll(table:String,sucess: @escaping (_ result: SQLiteQueryResult)->(),failure: @escaping (_ error: Error)->()){
        do{
            let result = try database.query("delete from \(table);")
            sucess(result)
        } catch let error {
            failure(error)
        }
        
    }
    
    func updateById(table:String, values:[String], id:String,sucess: @escaping (_ result: SQLiteQueryResult)->(),failure: @escaping (_ error: Error)->()){
        do{
            let result = try database.query("UPDATE \(table) SET id = '\(values[0])', type = '\(values[1])' WHERE id = '\(values[0])'")
            sucess(result)
        } catch let error {
            failure(error)
        }
    }
    
    func deleteById(table:String ,id:String,sucess: @escaping (_ result: SQLiteQueryResult)->(),failure: @escaping (_ error: Error)->()){
        do{
            let result = try database.query("DELETE FROM \(table) WHERE id = '\(id)'")
            sucess(result)
        } catch let error {
            failure(error)
        }
    }
    
    func insert(table:String,params:[String],values:[String],sucess: @escaping (_ result: SQLiteQueryResult)->(),failure: @escaping (_ error: Error)->()){
        
//        let stringParams = params.joined(separator: ",")
//        var stringValues = values.joined(separator: ",")
//        let queryInsert:String = "insert into \(table) (\(stringParams)) values (\(stringValues));"
        
        do{
            let result = try database.bindQuery("INSERT INTO 'ingredient' (id, type) VALUES (?,?)", bindValues: [sqlStr(values[0]),sqlStr(values[1])])
            sucess(result)
        } catch let error {
            failure(error)
        }
        
    }
    
}
